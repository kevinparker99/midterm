
public class PC {
	Case theCase;
	Monitor monitor;
	Motherboard motherboard;
	
	public PC(Case c, Monitor mon, Motherboard mom) {
		this.theCase = c;
		this.monitor = mon;
		this.motherboard = mom;
	}
	public Case getTheCase() {
		return this.theCase;
	}
}
