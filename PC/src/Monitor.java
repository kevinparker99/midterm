
public class Monitor {
	String model;
	String manufacturer;
	int size;
	Resolution resolution;
	
	public Monitor(String mod, String man, int s, Resolution res) {
		this.model = mod;
		this.manufacturer = man;
		this.size = s;
		this.resolution = res;
	}
}
