
public class Dimensions {
	int length;
	int width;
	int  height;
	public Dimensions(int l, int w, int h) {
		if (l<0) {
			this.length = 0;
		}else {
			this.length = l;
		}
		if (w<0) {
			this.width= 0;
		}else {
			this.width = w;
		}
		if (h<0) {
			this.height= 0;
		}else {
			this.height = h;
		}
		
	}
}
