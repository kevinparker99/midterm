
public class Case {
	Dimensions dimensions;
	String model;
	String manufacturer;
	String powerSupply;
	public Case(String mod, String man, String pow, Dimensions d) {
		this.model = mod;
		this.manufacturer = man;
		this.powerSupply = pow;
		this.dimensions = d;
	}
	public void pressPowerButton() {
		System.out.println("Power button pressed");
	}
}
