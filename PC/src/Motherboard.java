
public class Motherboard {
	String model;
	String manufacturer;
	int ramSlot;
	int cardSlots;
	String bios;
	public Motherboard(String mod, String man, int ram, int card, String b) {
		this.model = mod;
		this.manufacturer = man;
		this.ramSlot = ram;
		this.cardSlots = card;
		this.bios = b;
	}
}
