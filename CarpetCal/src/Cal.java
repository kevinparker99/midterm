
public class Cal {
 Floor floor;
 Carpet carpet;
 public Cal(Floor f, Carpet c) {
	 this.floor = f;
	 this.carpet = c;
 }
 public double getTotalCost() {
	 double total = this.floor.getArea()*this.carpet.getCost();
	 return total;
 }
}
