
public class Floor {
	double length;
	double width;
	
	public Floor(double l, double w) {
		if (l<0) {
			this.length = 0;
		}else {
			this.length = l;
		}
		if (w<0) {
			this.width= 0;
		}else {
			this.width = w;
		}
	}
	public double getArea() {
		return this.width*this.length;
	}
}
